#include <iostream>
#include "Definitions.h"
#include <sstream>
#include <stdlib.h>

using namespace std;

void* g_pKeyHandle = 0;
unsigned short g_usNodeId = 1;
const char* g_deviceName = "EPOS4";
const char* g_protocolStackName = "MAXON SERIAL V2";
const char* g_interfaceName = "RS232";
const char* g_portName = "COM1";
int g_baudrate = 115200;


void LogInfo(string message)
{
	cout << message << endl;
}


int OpenDevice(unsigned long & p_rlErrorCode)
{	
	LogInfo("Opening device...");
	g_pKeyHandle = VCS_OpenDevice((char*)g_deviceName, (char*)g_protocolStackName, (char*)g_interfaceName, (char*)g_portName, &p_rlErrorCode);
	return 1;
}


int CloseDevice(unsigned long & p_rlErrorCode)
{
	LogInfo("Closing device...");
	int lResult = VCS_CloseDevice(g_pKeyHandle, &p_rlErrorCode);
	return lResult;
}


 int main()
{
	int i = 0;
	WORD pinpst;
	unsigned long ulErrorCode = 0;
	OpenDevice(ulErrorCode);
	VCS_SetEnableState(g_pKeyHandle, g_usNodeId, &ulErrorCode);
	VCS_GetAllDigitalInputs(g_pKeyHandle, g_usNodeId, &pinpst, &ulErrorCode);
	cout << "INPUT NUMBER IS : " << pinpst << endl;
	VCS_SetDisableState(g_pKeyHandle, g_usNodeId, &ulErrorCode);
	CloseDevice(ulErrorCode);	
}