#include <iostream>
#include "Definitions.h"
#include <sstream>
#include <stdlib.h>
//#include <stdio.h>
//#include <bitset>

using namespace std;

void* g_pKeyHandle = 0;
unsigned short g_usNodeId = 1;
const char* g_deviceName = "EPOS4";
const char* g_protocolStackName = "MAXON SERIAL V2";
const char* g_interfaceName = "RS232";
const char* g_portName = "COM1";
int g_baudrate = 115200;


#ifndef MMC_SUCCESS
#define MMC_SUCCESS 0
#endif

#ifndef MMC_FAILED
#define MMC_FAILED 1
#endif

void LogInfo(string message)
{
	cout << message << endl;
}


int OpenDevice(unsigned long & p_rlErrorCode) //Open the comunication with the motor
{	
	LogInfo("Opening device...");
	g_pKeyHandle = VCS_OpenDevice((char*)g_deviceName, (char*)g_protocolStackName, (char*)g_interfaceName, (char*)g_portName, &p_rlErrorCode);
	int lResult = MMC_SUCCESS;
	return lResult;
}


int CloseDevice(unsigned long & p_rlErrorCode) //Close the comunication with the motor
{
	LogInfo("Closing device...");
	int lResult = VCS_CloseDevice(g_pKeyHandle, &p_rlErrorCode);
	return lResult;
}


  int DemoProfilePositionMode(HANDLE p_DeviceHandle, unsigned short p_usNodeId, unsigned long & p_rlErrorCode) // Tells the motor where to go and when to stop
{
	long targetPosition = 0; // Set the final position of the motor  
	long pt=0; // Position Check
	WORD pinpst;
	WORD pin=0;
	VCS_ActivateProfilePositionMode(p_DeviceHandle, p_usNodeId, &p_rlErrorCode); //Set the  Profile ''Position'' ON
	VCS_SetPositionProfile(p_DeviceHandle, p_usNodeId, 1000/*Motor_velocity*/, 1000/*Motor_Acceleration*/, 3000/*Motor_Deceleration*/, &p_rlErrorCode);// Send to the motor the data to perform the movement
	VCS_MoveToPosition(p_DeviceHandle, p_usNodeId, targetPosition, 1, 1, &p_rlErrorCode); //Make the motor to go to the target position
	if (targetPosition <= 0) //Set what happen for the negative limit "UP"
	{
		do
		{	
			VCS_GetAllDigitalInputs(g_pKeyHandle, g_usNodeId, &pinpst, &p_rlErrorCode);
			pin = pinpst;
			//cout << pin << endl;
			//cout << bitset<16>(p1)<< endl;
			long posis;
			VCS_GetPositionIs(g_pKeyHandle, g_usNodeId, &posis, &p_rlErrorCode);
			pt = posis;
		} while ((pin != 20992) && (pt != targetPosition));
		
		VCS_SetQuickStopState(g_pKeyHandle, g_usNodeId, &p_rlErrorCode);
	}
	else // Set what happen for the positive limit "DOWN" 
	{
		do
		{
			VCS_GetAllDigitalInputs(g_pKeyHandle, g_usNodeId, &pinpst, &p_rlErrorCode);
			pin = pinpst;
			//cout << pin << endl;
			//cout << bitset<16>(p1)<< endl;
			long posis;
			VCS_GetPositionIs(g_pKeyHandle, g_usNodeId, &posis, &p_rlErrorCode);
			pt = posis;
		} while (pin != 32768 && (pt != targetPosition));

		VCS_SetQuickStopState(g_pKeyHandle, g_usNodeId, &p_rlErrorCode);
	}

	int lResult = VCS_SetQuickStopState(g_pKeyHandle, g_usNodeId, &p_rlErrorCode);
	return lResult;
	}


int Demo(unsigned long & p_rlErrorCode)// Various Operative Instructions passed to the main
{
	int lResult = DemoProfilePositionMode(g_pKeyHandle, g_usNodeId, p_rlErrorCode);
	return lResult;
}

int main()
{
	unsigned long ulErrorCode = 0;
	OpenDevice(ulErrorCode);
	VCS_SetEnableState(g_pKeyHandle, g_usNodeId, &ulErrorCode);//Enable the motor to take instruction
	Demo(ulErrorCode);
	VCS_SetDisableState(g_pKeyHandle, g_usNodeId, &ulErrorCode);//Disable the motor to take instruction
	CloseDevice(ulErrorCode);	
}